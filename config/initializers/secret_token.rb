# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
BLOG::Application.config.secret_key_base = '1a78e836839e27d2cddd66bb0429b6af4ae0ff51de9936cdb9c62e04601195f98cca0e0645ca4c9c843758d3c56e5fd8fa67058ca3e9789714ae58ade1c76220'
